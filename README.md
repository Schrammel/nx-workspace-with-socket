## Iniciar o frontend.

```
npm start todos
```


## Backend em modo desenvolvimento
```
docker-compose -f dev.yml up
```


## Backend em modo producao
```
npm run build --prod backend
docker-compose -f prod.yml up
```

## Backend em modo producao com node_modules
```
npm run build --prod backend
docker-compose -f prod2.yml up
```
