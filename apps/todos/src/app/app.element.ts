import './app.element.css';
import io from 'socket.io-client';

export class AppElement extends HTMLElement {
  public static observedAttributes = [];

  constructor( private socket = io('http://localhost:3333') ) {
    super();
    socket.on('connect', () => {
     alert('connected');
    })
    socket.on('disconnect', () => {
      alert('disconnected');
    })
  }

  connectedCallback() {
    const title = 'todos';
    const button =  document.createElement('input');
    button.autocomplete="off"
    button.innerHTML ="Send";
    button.id = "input";
    this.innerHTML = `
    <head>
    </head>
    <body>
    Um alert será exibido quando conectado.
  </body>
    `;
    this
  }
}
customElements.define('todo-root', AppElement);
